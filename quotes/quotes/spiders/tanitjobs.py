import scrapy
from ..items import QuotesItem
from scrapy.http import Request 
import requests
from scrapy.http import TextResponse
 

class TanitjobsSpider(scrapy.Spider):
    name = 'tanitjobs'
    page_number = 2
    start_urls = ['https://www.tanitjobs.com/jobs'
       ]

    page = 0

    def parse(self, response):
        items = QuotesItem()

        #if self.page == 0:
        all_div_quotes = response.css(".listing-item__jobs")

        next_pa = response.css('.pagination-bottom .pad_right_small+ a').css('::attr(href)').extract()
        
        # if (len(next_pa)>0):
        #     print("#####################",next_pa)
        # else:
        #     print("*********************",next_pa)



            

        for quotes in all_div_quotes:
                
            location = quotes.css('.listing-item__info--item-location').css('::text').extract()
            link = quotes.css('.listing-item__title .link').css('::attr(href)').extract()

             

            # to scrapping details (others url)
            url = link[0]
            reponse = requests.get(url)
            resp = TextResponse(body=reponse.content, url=url)

            

            job_name = resp.css('.details-header__title').css('::text').extract()
            company_name = resp.css('.listing-item__info--item-company').css('::text').extract()
            candidates_number = resp.css('.applicants-num').css('::text').extract()
            vacant_jobs = resp.css('.col-md-4:nth-child(1) dd').css('::text').extract()
            desired_job_type = resp.css('.col-md-4:nth-child(2) dd').css('::text').extract()
            experience = resp.css('.col-md-4:nth-child(3) dd').css('::text').extract()
            level_of_study = resp.css('.col-md-4:nth-child(4) dd').css('::text').extract()
            proposed_remuneration = resp.css('.col-md-4:nth-child(5) dd').css('::text').extract()
            language = resp.css('.col-md-4:nth-child(6) dd').css('::text').extract()
            gender = resp.css('.col-md-4:nth-child(7) dd').css('::text').extract()
            description = resp.css('.content-text:nth-child(3)').css('::text').extract()
            job_requirements = resp.css('.content-text:nth-child(5)').css('::text').extract()
            expiration_date = resp.css('.content-text:nth-child(7)').css('::text').extract()
            keywords = resp.css('.label-info').css('::text').extract()

            

            items['job_name'] = job_name
            items['company_name'] = company_name
            items['candidates_number'] = candidates_number
            items['vacant_jobs'] = vacant_jobs
            items['desired_job_type'] = desired_job_type
            items['experience'] = experience
            items['level_of_study'] = level_of_study
            items['proposed_remuneration'] = proposed_remuneration
            items['language'] = language
            items['gender'] = gender
            items['description'] = description
            items['job_requirements'] = job_requirements
            items['expiration_date'] = expiration_date
            items['keywords'] = keywords
            items['link'] = link
            items['location'] = location

               
                
                #self.i = 1

                #yield response.follow(link, callback = self.parse)
                # items['product_price'] = product_price
                # items['product_imagelink'] = product_imagelink

            # df = pd.DataFrame({'product_name': product_name,
            #            'product_author': product_author,
            #            'product_price': product_price,
            #             'product_imagelink' : product_imagelink})
            # df.to_csv("ghaziiiiii.csv",index=False)


            yield items

        


        next_page = 'https://www.tanitjobs.com/jobs?searchId=1599306981.6726&action=search&page='+ str(TanitjobsSpider.page_number)
        if (len(next_pa)>0):
            TanitjobsSpider.page_number += 1
            yield response.follow(next_page, callback = self.parse)



            
         