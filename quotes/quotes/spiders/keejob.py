import scrapy
from ..items import QuotesItem
import requests
from scrapy.http import TextResponse


class KeejobSpider(scrapy.Spider):
    name = 'keejob'
    page_number = 2
    start_urls = ['http://www.keejob.com/offres-emploi/']
    base_url = 'http://www.keejob.com'

    def parse(self, response):
        items = QuotesItem()

        all_div_quotes = response.css("#loop-container .clearfix")

        next_pa = response.css(
            '.page-item:nth-child(13) .page-link').css('::attr(href)').extract()

        # if (len(next_pa)>0):
        #     print("#####################",next_pa)
        # else:
        #     print("*********************",next_pa)

        for quotes in all_div_quotes:

            company_name = quotes.css(
                '.no-margin-left b').css('::text').extract()
            link = quotes.css('h6 a').css('::attr(href)').extract()
            publication_date = quotes.css(
                'span.pull-left').css('::text').extract()

            url = self.base_url+link[0]
            # print("*********************",url)
            reponse = requests.get(url)
            resp = TextResponse(body=reponse.content, url=url)

            job_name = resp.css('.job-title').css('::text').extract()
            candidates_number = []
            vacant_jobs = []

            proposed_remuneration = []

            gender = []
            description = resp.css(
                '.block_a.no-margin-left').css('::text').extract()
            job_requirements = []
            expiration_date = []
            keywords = []

            description_company = resp.css(
                '#main .content').css('::text').extract()

            i = 4

            desired_job_type = []
            location = []
            experience = []
            level_of_study = []
            employee_availability = []
            mobility = []
            language = []
            driver_license = []

            while i < 12:
                # print("iiiiiiiiiiiiiiiiiiiiiiiiii",i)
                
                result = resp.css(
                    '.meta:nth-child(' + str(i)+')').css('::text').extract()
                # print("--------++++++++++",result)
                if (len(result) > 1):
                    if(result[1] == 'Type de poste:'):
                        desired_job_type = result
                    if(result[1] == 'Lieu de travail:'):
                        location = result
                    if(result[1] == 'Expérience:'):
                        experience = result
                    if(result[1] == 'Étude:'):
                        level_of_study = result
                    if(result[1] == 'Disponibilité:'):
                        employee_availability = result
                    if(result[1] == 'Mobilité:'):
                        mobility = result
                    if(result[1] == 'Langues:'):
                        language = result
                    i += 1
                else:
                    if (len(result) > 0):
                        if(result[0] == 'Permis de conduire nécessaire'):
                            driver_license = result
                        i += 1
                    else:
                        i = 12

            #desired_job_type = resp.css('.meta:nth-child(4)').css('::text').extract()
            #experience = resp.css('.meta:nth-child(6)').css('::text').extract()
            #location = resp.css('.meta:nth-child(5)').css('::text').extract()
            #level_of_study = resp.css('.meta:nth-child(7)').css('::text').extract()
            #language = resp.css('.meta:nth-child(8)').css('::text').extract()
            #employee_availability = resp.css('.meta:nth-child(8)').css('::text').extract()

            items['job_name'] = job_name
            items['company_name'] = company_name
            items['candidates_number'] = candidates_number
            items['vacant_jobs'] = vacant_jobs
            if(len(desired_job_type) > 0):
                items['desired_job_type'] = desired_job_type[3]
            else:
                items['desired_job_type'] = desired_job_type

            if(len(experience) > 0):
                items['experience'] = experience[3]
            else:
                items['experience'] = experience

            if(len(level_of_study) > 0):
                items['level_of_study'] = level_of_study[3]
            else:
                items['level_of_study'] = level_of_study
            items['proposed_remuneration'] = proposed_remuneration
            if(len(language) > 0):
                items['language'] = language[3]
            else:
                items['language'] = language
            items['gender'] = gender
            if(len(description)>2):
                items['description'] = description[3:]
            else:
                items['description'] = description
            items['job_requirements'] = job_requirements
            items['expiration_date'] = expiration_date
            items['keywords'] = keywords
            items['link'] = self.base_url+link[0]
            if(len(location) > 0):
                items['location'] = location[3]
            else:
                items['location'] = location
            items['publication_date'] = publication_date[1]
            if(len(employee_availability) > 0):
                items['employee_availability'] = employee_availability[3]
            else:
                items['employee_availability'] = employee_availability
            items['description_company'] = description_company
            if(len(mobility)>0):
                items['mobility'] = mobility[3]
            else:
                items['mobility'] = mobility
            items['driver_license'] = driver_license

            yield items

        if (len(next_pa)>0):
            next_page = 'https://www.keejob.com/offres-emploi/'+ next_pa[0]
            # print("##############",next_pa)
            # print("-------------------",next_page)
            # TanitjobsSpider.page_number += 1
            yield response.follow(next_page, callback = self.parse)
