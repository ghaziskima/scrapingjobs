import scrapy
from ..items import QuotesItem
import requests
from scrapy.http import TextResponse

class BaytSpider(scrapy.Spider):
    name = 'bayt'
    start_urls = ['http://www.bayt.com/fr/tunisia/jobs/']
    base_url = 'https://www.bayt.com/fr/tunisia/jobs/'

    def parse(self, response):
        items = QuotesItem() 
        all_div_quotes = response.css(".has-pointer-d")


        next_pa = response.css(
            '.pagination-next .jsAjaxLoad').css('::attr(href)').extract()


        

        for quotes in all_div_quotes:
            link = quotes.css('a').css('::attr(href)').extract()
            job_name = quotes.css('a').css('::text').extract()

            url = self.base_url+link[0]
            reponse = requests.get(url)
            resp = TextResponse(body=reponse.content, url=url)

 
            company_name = resp.css('li > .is-black span').css('::text').extract()
            # location = resp.css('span .is-black span').css('::text').extract()
            description = resp.css('.is-spaced div p').css('::text').extract()
            required_profile = resp.css('.print-break-before p').css('::text').extract()
            description_company = resp.css('.p20t p').css('::text').extract()

            

            

            

            resultTr = resp.css('dl div')

            location = []
            activity_area = []
            job = []
            job_requirements = []
            proposed_remuneration = []
            vacant_jobs = []
            level_of_study = []
            

            for tr in resultTr:
    
                first_cell = tr.css('dt').css('::text').extract()
                last_cell = tr.css('dd').css('::text').extract()
                
                if (len(first_cell) > 0):
                    if(first_cell[0] == 'Lieu de travail'):
                        location = last_cell
                    if(first_cell[0] == 'Domaine de la société'):
                        activity_area = last_cell
                    if(first_cell[0] == 'Fonction'):
                        job = last_cell
                    if(first_cell[0] == "Type d'emploi"):
                        job_requirements = last_cell
                    if(first_cell[0] == 'Salaire mensuel'):
                        proposed_remuneration = last_cell
                    if(first_cell[0] == "Nombre de postes à pourvoir'"):
                        vacant_jobs = last_cell
                    if(first_cell[0] == "Niveau de carrière'"):
                        level_of_study = last_cell

            items['location'] = location
            items['activity_area'] = activity_area
            items['job'] = job
            items['link'] = self.base_url+link[0]

            items['job_requirements'] = job_requirements
            items['proposed_remuneration'] = proposed_remuneration
            items['vacant_jobs'] = vacant_jobs
            items['level_of_study'] = level_of_study
            items['job_name'] = job_name
            items['company_name'] = company_name
            items['description'] = description
            items['required_profile'] = required_profile
            items['description_company'] = description_company

            yield items

        if (len(next_pa)>0):
            next_page = "https://www.bayt.com"+ next_pa[0]
            print("##############",next_page)
            # print("-------------------",next_page)
            # TanitjobsSpider.page_number += 1
            yield response.follow(next_page, callback = self.parse)

           
            

