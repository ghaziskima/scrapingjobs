import scrapy
from ..items import QuotesItem
import requests
from scrapy.http import TextResponse


class EmploitunisieSpider(scrapy.Spider):
    name = 'emploitunisie'
    start_urls = ['https://www.emploitunisie.com/recherche-jobs-tunisie']
    base_url = 'http://www.emploitunisie.com'

    def parse(self, response):

        items = QuotesItem()

        

        all_div_quotes = response.css("#jobsearch-search-results-box .row")

         
        

        next_pa = response.css(
            '#jobsearch-search-results-box .last a').css('::attr(href)').extract()

        for quotes in all_div_quotes:

            company_name_with_publication_date = quotes.css(
                '.job-recruiter').css('::text').extract()
            link = quotes.css('h5 a').css('::attr(href)').extract()
            job_name = quotes.css('h5 a').css('::text').extract()



            url = self.base_url+link[0]
            reponse = requests.get(url)
            resp = TextResponse(body=reponse.content, url=url)


            description = resp.css('.jobs-ad-details .clearfix > div:nth-child(1)').css('::text').extract()
            description_company = resp.css('.job-ad-company-description').css('::text').extract()
            required_profile =  resp.css('.job-ad-separator+ div p').css('::text').extract()

                            
           # desired_job_type = resp.css('.field-name-field-offre-contrat-type .even , .job-ad-criteria tr:nth-child(3) td:nth-child(1)').css('::text').extract()




            # i = 1

            # while i < 10:
            #     result = resp.css(
            #         '.tr:nth-child(' + str(i)+')').css('::text').extract()
            #     print('######///////####',result)
            #     if (len(result) > 1):
            #         # if(result[1] == 'Type de poste:'):
                    #     desired_job_type = result
                    # if(result[1] == 'Lieu de travail:'):
                    #     location = result
                    # if(result[1] == 'Expérience:'):
                    #     experience = result
                    # if(result[1] == 'Étude:'):
                    #     level_of_study = result
                    # if(result[1] == 'Disponibilité:'):
                    #     employee_availability = result
                    # if(result[1] == 'Mobilité:'):
                    #     mobility = result
                    # if(result[1] == 'Langues:'):
                    #     language = result

                    #print('######///////####',result)

                  #  i += 1
                 
            
            website = []
            job = []
            activity_area = []
            desired_job_type = []
            location = []
            experience = []
            level_of_study = []

            vacant_jobs = []
            language = []

            

            # experience = resp.css('tr:nth-child(6)').css('::text').extract()
            resultTr = resp.css('tr')

            for tr in resultTr:

                first_cell = tr.css('td:nth-child(1)').css('::text').extract()
                last_cell = tr.css('td+ td , .odd , .even').css('::text').extract()

                if (len(first_cell) > 0):
                    if(first_cell[0] == 'Site Internet : '):
                        website = last_cell
                    if(first_cell[0] == 'Secteur d´activité : '):
                        activity_area = last_cell
                    if(first_cell[0] == 'Métier : '):
                        job = last_cell
                    if(first_cell[0] == 'Type de contrat : '):
                        desired_job_type = last_cell
                    if(first_cell[0] == 'Ville : '):
                        location = last_cell
                    if(first_cell[0] == "Niveau d'expérience : "):
                        experience = last_cell
                    if(first_cell[0] == "Niveau d'études : "):
                        level_of_study = last_cell
                    if(first_cell[0] == 'Langues exigées : '):
                        language = last_cell
                    if(first_cell[0] == 'Nombre de poste(s) : '):
                        vacant_jobs = last_cell


                

            
            
             
           

            
            items['job_name'] = job_name
            items['company_name'] = company_name_with_publication_date[1]
            items['publication_date'] = company_name_with_publication_date[0]
            items['link'] = self.base_url+link[0]

            items['website'] = website
            items['job'] = job
            items['activity_area'] = activity_area
            items['desired_job_type'] = desired_job_type
            items['location'] = location
            items['experience'] = experience
            items['level_of_study'] = level_of_study
            items['vacant_jobs'] = vacant_jobs
            items['language'] = language
            items['description'] = description
            items['description_company'] = description_company
            items['required_profile'] = required_profile

  


            

            yield items

        if (len(next_pa)>0):
            next_page = self.base_url+ next_pa[0]
            # print("##############",next_pa)
            # print("-------------------",next_page)
            # TanitjobsSpider.page_number += 1
            yield response.follow(next_page, callback = self.parse)

        



        
