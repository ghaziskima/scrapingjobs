# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join
from w3lib.html import remove_tags
def remove_nt(value):
    return value.replace("\n",'').replace(" ","")


class QuotesItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    job_name = scrapy.Field()
    company_name = scrapy.Field()
    location = scrapy.Field()
    link = scrapy.Field()
    candidates_number = scrapy.Field()
    vacant_jobs = scrapy.Field()
    desired_job_type = scrapy.Field()
    experience = scrapy.Field()
    level_of_study = scrapy.Field()
    proposed_remuneration = scrapy.Field()
    language = scrapy.Field()
    gender = scrapy.Field()
    description = scrapy.Field()
    job_requirements = scrapy.Field()
    expiration_date = scrapy.Field()
    keywords = scrapy.Field()
    #---------
    publication_date = scrapy.Field()
    employee_availability = scrapy.Field()
    description_company = scrapy.Field()
    mobility = scrapy.Field()
    driver_license = scrapy.Field()

    #----------
    website = scrapy.Field()
    job = scrapy.Field()
    activity_area = scrapy.Field()   # secteur d'activité
    required_profile = scrapy.Field()




 